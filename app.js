// load .env config
require('dotenv').config()

// whatsapp web client
const { Client } = require('whatsapp-web.js')
const client = new Client({
    ...(process.env.CLIENT_SESSION && { session: JSON.parse(process.env.CLIENT_SESSION) })
})
const qrcode = require('qrcode-terminal')

client.on('qr', qr => {
    console.log("qr code received")
    qrcode.generate(qr, { small: true })
})

client.on('ready', () => {
    console.log("client is ready.")
})

client.on('message', async message => {
    const chat = await message.getChat()
    const contact = await chat.getContact()

    console.log("new message", message)
    if(message.body === '!ping') {
        message.reply("pong")
    }

    console.log("phone id", chat.id.user, contact)

    chat.sendMessage(`Hello ${contact.pushname}`)
})

client.on('authenticated', session => {
    if(process.env.CLIENT_SESSION) {
        console.log("session restored")
    } else {
        console.log("session", JSON.stringify(session))
    }
})

client.initialize()